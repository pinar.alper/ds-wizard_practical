# Data Stewardship Wizard - Practical

The Data Stewardship Wizard (hereon DS Wizard) is an awareness tool in the area of data management. In this practical we will cover some basic use-cases that the DS Wizard supports.

## Required resources

In order to run this practical you need the following resources, which will be provided by the session instructor.
 
* Two usernames in order to be able to login to the applications as a _data steward_ and as a _researcher_. 
* "Human Data Submission Checklist" of the journal _Nature Scientific Data_.
* The URL link for accessing the training instance of the DS Wizard.

## Exercise 1: Extend an existing knowledge model
### PART I
To begin this exercise you need a knowledge model to work on. Instead of starting with an empty model, you will extend an existing model that has been created for this training. To do so:

1. Login to the DS Wizard as a _data steward_.
2. On the left-hand side navigation menu click _Knowledge Models_ to list all published models.
3. From the models listed, click on the _View details_ link next to the model named _Data Protection_, as depicted below. <br/> ![Alt](./images/list_base_km.png)<br/><br/>
4. You will be taken to the quick view page for the _Data Protection_ model. From the action menu on top right corner, click _Fork knowledge model_, as seen below. <br/> ![Alt](./images/fork_base_km.png)<br/><br/>
5. You will see the _Create Knowledge Model_ page, where you will be asked to provide a name for your model. Please append your DS Wizard username to the model name, as illustrated below. This will later help in listing models created by all trainees. 
  ![Alt](./images/create_km.png)<br/><br/>
6. Click _Save_ and you will be taken to the editor page for your new model.

### PART II

You should now have a copy of the knowledge model that is based on the "Human Data Submission Checklist" of Nature Scientific Data. If you browse through the sections of the model, you will realise that it is not yet complete. Particularly, **"Section IV – DATA ACCESS AFTER PUBLICATION"** contains no questions. In this exercise you should add two questions to this section using the checklist document as your guide. To do so:


1. Use the Knowledge Model editor to add questions under "Section IV – DATA ACCESS AFTER PUBLICATION". 
    * **TIP:** When making changes to the model **it is more efficient to press SAVE once you have made all changes e.g. added all questions**. Your changes are being captured when you browse through different sections. Pressing SAVE will take you to the knowledge models listing page.
    * **TIP:** When adding  questions <mark>pay attention to the question types</mark> (screenshot below). Each type will determine how the question can be answered:
        - **Value**: The user will provide the answer as a single value, either a _date_, a _numeric_ or a _textual_ value. As part of this exercise you will be creating question with a _textual value_ answer.
        - **Option**: The user will be shown a list of possible answers, and she will be able to select only one. Most questions in our example knowledge model are of this category. Within this exercise you will be creating a question with YES/NO options as answer.
        - **List of items**: This category is a container for creating sub-questions. It is up to you how to design sub-questions. E.g. Make each  sub-question an option would be one way to implement a question, where the users can select multiple options in response to the parent question. You will not need to use this category is not explored in this training.
        - **Integration**: This category is used to dynamically obtain possible answers for a question from a URL end point. To use this option you must create an Integration definition first. You will not need to use this category is not explored in this training. <br/> ![Alt](./images/question_types.png)<br/><br/>
    * **TIP:** When adding questions <mark>pay attention to when this question will become desirable</mark> (screenshot below). With this feature you can filter questions in very large knowledge models. For this exercise, i.e. "Human Data Submission Checklist" model, all questions become desirable in the same phase of the project, which is **"Before finishing the project"**. This is the stage when project findings and data are published in relevant journals. Make sure you select this option for all questions that you add. <br/> ![Alt](./images/question_stages.png)<br/><br/>

2. Once you have saved your extended knowledge model you will see it listed in the _KM Editor_ section as follows. Click the _Publish_ link next to your model. <br/> ![Alt](./images/list_saved_km.png)<br/><br/>
3. You will be taken to the publishing details page, where you'd need to provide a version number for your model. Provide this information and click _Publish_.
4. Once your model is published it will be listed, as below, in the _Knowledge Models_ section. Click on the _View detail_ link next to your model. <br/> ![Alt](./images/list_published_km.png)<br/><br/>
6. You will be taken to the quick view page for your model, as seen below. From the action menu on top right corner, click _Create Questionnaire_. <br/> ![Alt](./images/create_questionnaire.png)<br/><br/>
7. You will see the _Create Questionnaire_ page where you will be asked to provide a name for your model. Please append your DS Wizard username as a suffix to the questionnaire name. You will also be asked for an accessibility setting, you can select _Public_ (illustrated below). <br/> ![Alt](./images/create_questionnaire_dialog.png)<br/><br/>
5. Once you click _Save_, you will be taken to the Questionnaire fill-in page, which we will describe in the next Exercise. 
6. Log out of the DS Wizard by clicking on your user name at the bottom right corner and selecting _Logout_.


## Exercise 2: Fill-in and export a questionnaire
1. Login to the DS Wizard as a _researcher_.
2. On the left-hand side navigation menu click _Questionnaires_ to list all questionnaires visible to you. You should see the Questionnaire you have created in Exercise 1, as below. Click the _Fill questionnaire_ link next to your questionnaire. <br/> ![Alt](./images/fill_questionnaire.png)<br/><br/>
3. You will be taken to the Questionnaire fill-in page. Provide answers to the questions in the "Human Data Submission Checklist" and do not forget to click _Save_ at the end. 
4. You can export your answers to a questionnaire by clicking on the export link next to the questionnaire. <br/> ![Alt](./images/export_questionnaire.png)<br/><br/> You will be able to export in a number of formats, as seen below. <br/> ![Alt](./images/export_questionnaire_dialog.png)<br/><br/>
